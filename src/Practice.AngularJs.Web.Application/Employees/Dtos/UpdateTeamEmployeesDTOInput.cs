using System.Collections.Generic;
using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class UpdateTeamEmployeesDTOInput : IInputIDTO
    {
        public int TeamId { get; set; }

        public List<int> EmployeeIds { get; set; }
    }
}