﻿using System.Collections.Generic;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
	public class EmployeeDTO
	{
		public int Id { get; set; }

		
		public string Name { get; set; }


        public string Job { get; set; }

	    
        public decimal Age { get; set; }


	    public ICollection<SkillDTO> Skills { get; set; }


		public ICollection<TeamDTO> Teams { get; set; }


        public ICollection<FeedbackDTO> Feedbacks { get; set; }

	}
}