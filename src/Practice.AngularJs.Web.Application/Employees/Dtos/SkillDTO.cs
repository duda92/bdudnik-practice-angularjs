using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class SkillDTO : IDTO
    {
        public int Id { get; set; }

        public string Grade { get; set; }

        public SpecializationDTO Specialization { get; set; }
    }
}