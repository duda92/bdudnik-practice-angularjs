using System.Collections.Generic;
using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class TeamListDTOOutput : IOutputDTO
    {
        public List<TeamDTO> Teams { get; set; }
    }
}