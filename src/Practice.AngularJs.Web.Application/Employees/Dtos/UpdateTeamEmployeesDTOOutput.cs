using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class UpdateTeamEmployeesDTOOutput : IOutputDTO
    {
        public TeamDTO Team { get; set; }
    }
}