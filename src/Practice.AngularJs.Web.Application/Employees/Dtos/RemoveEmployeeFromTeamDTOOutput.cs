using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
	public class RemoveEmployeeFromTeamDTOOutput : IOutputDTO
	{
		public TeamDTO Team { get; set; }
	}
}