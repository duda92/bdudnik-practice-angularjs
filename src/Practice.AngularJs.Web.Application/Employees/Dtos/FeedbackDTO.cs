using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class FeedbackDTO : IDTO
    {
        public int Id { get; set; }

        public string Text { get; set; }
    }
}