using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class NewTeamDTOOutput : IOutputDTO
    {
        public TeamDTO Team { get; set; }
    }
}