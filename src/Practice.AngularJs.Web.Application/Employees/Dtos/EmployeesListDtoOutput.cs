﻿using System.Collections.Generic;
using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
	public class EmployeesListDTOOutput : IOutputDTO
	{
		public List<EmployeeDTO> Employees { get; set; }
	}
}