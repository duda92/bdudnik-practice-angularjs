using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class SpecializationDTO : IDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}