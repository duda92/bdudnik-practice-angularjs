using System.Collections.Generic;
using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class TeamDTO : IDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public ICollection<EmployeeDTO> Members { get; set; }
    }
}