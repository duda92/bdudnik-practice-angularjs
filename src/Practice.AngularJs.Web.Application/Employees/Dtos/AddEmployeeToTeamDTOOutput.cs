using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
	public class AddEmployeeToTeamDTOOutput : IOutputDTO
	{
		public TeamDTO Team { get; set; }
	}
}