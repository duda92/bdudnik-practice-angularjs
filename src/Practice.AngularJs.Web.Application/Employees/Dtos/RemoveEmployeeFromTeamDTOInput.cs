using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
	public class RemoveEmployeeFromTeamDTOInput : IInputIDTO
	{
		public int TeamId { get; set; }

		public int EmployeeId { get; set; }
	}
}