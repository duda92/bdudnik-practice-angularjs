using Practice.AngularJs.Core.SuperLayerTypes;

namespace Practice.AngularJs.Web.Application.Employees.DTOs
{
    public class NewTeamDTOInput : IInputIDTO
    {
        public string Name { get; set; }
    }
}