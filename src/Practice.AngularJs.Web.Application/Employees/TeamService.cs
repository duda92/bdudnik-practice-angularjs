using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Practice.AngularJs.Domain.Employees;
using Practice.AngularJs.Domain.Teams;
using Practice.AngularJs.Web.Application.Employees.DTOs;

namespace Practice.AngularJs.Web.Application.Employees
{
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public TeamService(ITeamRepository teamRepository, IEmployeeRepository employeeRepository)
        {
            _teamRepository = teamRepository;
            _employeeRepository = employeeRepository;
        }

        public TeamListDTOOutput GetTeamsList(GetTeamListDTOInput getEmployeeListDTOInput)
        {
            List<Team> teams = _teamRepository.GetAll().ToList();
            return new TeamListDTOOutput
            {
                Teams = Mapper.Map<List<TeamDTO>>(teams)
            };
        }

        NewTeamDTOOutput ITeamService.NewTeam(NewTeamDTOInput newTeamDTOInput)
        {
            Team team = Mapper.Map<Team>(newTeamDTOInput);
            
            _teamRepository.Add(team);
            _teamRepository.Save();

            return new NewTeamDTOOutput()
            {
                Team = Mapper.Map<TeamDTO>(team)
            };
        }

        public UpdateTeamEmployeesDTOOutput UpdateTeamEmployees(UpdateTeamEmployeesDTOInput updateTeamEmployeesDTOInput)
        {
            int teamId = updateTeamEmployeesDTOInput.TeamId;
            Team team = _teamRepository.FindBy(x => x.Id == teamId).Single();

            List<int> employeeIds = updateTeamEmployeesDTOInput.EmployeeIds;
            
			_teamRepository.SetMembers(team.Id, employeeIds);
            
			return new UpdateTeamEmployeesDTOOutput
            {
                Team = Mapper.Map<TeamDTO>(team)
            };
        }

	    public AddEmployeeToTeamDTOOutput AddEmployeeToTeam(AddEmployeeToTeamDTOInput addEmployeeToTeamDTOInput)
	    {
			int teamId = addEmployeeToTeamDTOInput.TeamId;
			int employeeId = addEmployeeToTeamDTOInput.EmployeeId;
			_teamRepository.AddMember(teamId, employeeId);
			_teamRepository.Save();

			Team team = _teamRepository.FindBy(x => x.Id == teamId).Single();
		    return new AddEmployeeToTeamDTOOutput()
		    {
			    Team = Mapper.Map<TeamDTO>(team)
		    };
	    }

	    public RemoveEmployeeFromTeamDTOOutput RemoveEmployeeFromTeam(RemoveEmployeeFromTeamDTOInput removeEmployeeFromTeamDTOInput)
	    {
			int teamId = removeEmployeeFromTeamDTOInput.TeamId;
			int employeeId = removeEmployeeFromTeamDTOInput.EmployeeId;
			_teamRepository.RemoveMember(teamId, employeeId);
			_teamRepository.Save();

			Team team = _teamRepository.FindBy(x => x.Id == teamId).Single();
			return new RemoveEmployeeFromTeamDTOOutput()
			{
				Team = Mapper.Map<TeamDTO>(team)
			};
	    }
    }
}