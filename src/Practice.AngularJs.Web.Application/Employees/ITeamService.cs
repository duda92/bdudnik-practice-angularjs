using Practice.AngularJs.Web.Application.Employees.DTOs;

namespace Practice.AngularJs.Web.Application.Employees
{
	public interface ITeamService
	{
		TeamListDTOOutput GetTeamsList(GetTeamListDTOInput getEmployeeListDTOInput);
		NewTeamDTOOutput NewTeam(NewTeamDTOInput newTeamDTOInput);
		UpdateTeamEmployeesDTOOutput UpdateTeamEmployees(UpdateTeamEmployeesDTOInput updateTeamEmployeesDTOInput);
		AddEmployeeToTeamDTOOutput AddEmployeeToTeam(AddEmployeeToTeamDTOInput addEmployeeToTeamDTOInput);
		RemoveEmployeeFromTeamDTOOutput RemoveEmployeeFromTeam(RemoveEmployeeFromTeamDTOInput removeEmployeeFromTeamDTOInput);
	}
}