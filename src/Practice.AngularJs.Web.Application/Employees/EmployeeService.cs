﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Practice.AngularJs.Domain.Employees;
using Practice.AngularJs.Web.Application.Employees.DTOs;

namespace Practice.AngularJs.Web.Application.Employees
{
	public class EmployeeService : IEmployeeService
	{
		private readonly IEmployeeRepository _employeeRepository;

		public EmployeeService(IEmployeeRepository employeeRepository)
		{
			_employeeRepository = employeeRepository;
		}

		public EmployeesListDTOOutput GetEmployeeList(GetEmployeeListInput getEmployeeListInput)
		{
			List<Employee> employees = _employeeRepository.GetAll().ToList();
			return new EmployeesListDTOOutput
			{
				Employees = Mapper.Map<List<EmployeeDTO>>(employees)
			};
		}
	}
}