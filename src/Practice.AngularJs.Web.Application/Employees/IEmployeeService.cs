﻿using Practice.AngularJs.Core.SuperLayerTypes;
using Practice.AngularJs.Web.Application.Employees.DTOs;

namespace Practice.AngularJs.Web.Application.Employees
{
	public interface IEmployeeService : IApplicationService
	{
		EmployeesListDTOOutput GetEmployeeList(GetEmployeeListInput getEmployeeListInput);
	}
}