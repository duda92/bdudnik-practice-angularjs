﻿using Autofac;

namespace Practice.AngularJs.Web.Application
{
	public class PracticeAngularJsWebApplicationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			IoCConfig.Configure(builder);
			MapperConfig.Configure();
		}
	}
}
