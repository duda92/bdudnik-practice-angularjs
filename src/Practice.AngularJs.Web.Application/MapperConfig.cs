﻿using AutoMapper;
using Practice.AngularJs.Domain.Employees;
using Practice.AngularJs.Domain.Skills;
using Practice.AngularJs.Domain.Teams;
using Practice.AngularJs.Web.Application.Employees.DTOs;

namespace Practice.AngularJs.Web.Application
{
	public static class MapperConfig
	{
		public static void Configure()
		{
			Mapper.CreateMap<Employee, EmployeeDTO>().ForMember(
                dest => dest.Job,
                opt => opt.MapFrom(src => src.Job.Title));
            Mapper.CreateMap<Team, TeamDTO>();
            Mapper.CreateMap<NewTeamDTOInput, Team>().ForMember(
                dest => dest.Title,
                opt => opt.MapFrom(src => src.Name));
		    Mapper.CreateMap<Skill, SkillDTO>().ForMember(
                dest => dest.Grade,
                opt => opt.MapFrom(src => src.Grade.ToString()));
		    Mapper.CreateMap<Specialization, SpecializationDTO>();
			Mapper.CreateMap<Feedback, FeedbackDTO>();

		}
	}
}