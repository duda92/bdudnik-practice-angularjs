﻿using Autofac;
using Practice.AngularJs.Domain.Employees;
using Practice.AngularJs.Domain.Skills;
using Practice.AngularJs.Domain.Teams;
using Practice.AngularJs.Infrastructure.DataAccess.EF.Employees;
using Practice.AngularJs.Infrastructure.DataAccess.EF.Skills;
using Practice.AngularJs.Infrastructure.DataAccess.EF.Teams;

namespace Practice.AngularJs.Web.Application
{
	public static class IoCConfig
	{
		public static void Configure(ContainerBuilder builder)
		{
			builder.RegisterType<TeamRepository>().As<ITeamRepository>();
			builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>();
            builder.RegisterType<SpecializationRepository>().As<ISpecializationRepository>();
		}
	}
}