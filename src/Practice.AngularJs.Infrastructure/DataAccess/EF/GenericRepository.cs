﻿using System;
using System.Data.Entity;
using System.Linq;
using Practice.AngularJs.Domain;

namespace Practice.AngularJs.Infrastructure.DataAccess.EF
{
	public abstract class GenericRepository<T, TC> : IGenericRepository<T>
		where T : class
		where TC : DbContext, new()
	{

		private TC _entities = new TC();
		public TC Context
		{

			get { return _entities; }
			set { _entities = value; }
		}

		public virtual IQueryable<T> GetAll()
		{

			IQueryable<T> query = _entities.Set<T>();
			return query;
		}

        public virtual IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
		{

			IQueryable<T> query = _entities.Set<T>().Where(predicate);
			return query;
		}

		public virtual void Add(T entity)
		{
			_entities.Set<T>().Add(entity);
		}

		public virtual void Delete(T entity)
		{
			_entities.Set<T>().Remove(entity);
		}

		public virtual void Edit(T entity)
		{
			_entities.Entry(entity).State = EntityState.Modified;
		}

		public virtual void Save()
		{
			_entities.SaveChanges();
		}
	}
}