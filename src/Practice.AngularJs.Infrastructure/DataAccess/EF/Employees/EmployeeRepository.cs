﻿using System.Data.Entity;
using System.Linq;
using Practice.AngularJs.Domain.Employees;

namespace Practice.AngularJs.Infrastructure.DataAccess.EF.Employees
{
	public class EmployeeRepository : GenericRepository<Employee, DatabaseContext>, IEmployeeRepository
	{
	    public override IQueryable<Employee> GetAll()
	    {
	        return base.GetAll().Include(x => x.Skills).Include(x => x.Feedbacks).Include(x => x.Job);
	    }
	}
}
