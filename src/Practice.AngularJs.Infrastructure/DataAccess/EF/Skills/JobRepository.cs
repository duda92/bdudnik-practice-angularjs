﻿using Practice.AngularJs.Domain.Employees;
using Practice.AngularJs.Domain.Skills;

namespace Practice.AngularJs.Infrastructure.DataAccess.EF.Skills
{
    public class JobRepository : GenericRepository<Job, DatabaseContext>, IJobRepository
    {
    }
}