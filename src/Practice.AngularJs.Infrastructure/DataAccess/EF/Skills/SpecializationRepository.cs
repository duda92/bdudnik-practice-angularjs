﻿using Practice.AngularJs.Domain.Skills;

namespace Practice.AngularJs.Infrastructure.DataAccess.EF.Skills
{
    public class SpecializationRepository : GenericRepository<Specialization, DatabaseContext>, ISpecializationRepository
    {
    }
}
