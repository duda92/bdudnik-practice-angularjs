﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Practice.AngularJs.Domain.Employees;
using Practice.AngularJs.Domain.Teams;

namespace Practice.AngularJs.Infrastructure.DataAccess.EF.Teams
{
	public class TeamRepository : GenericRepository<Team, DatabaseContext>, ITeamRepository
	{
		public override IQueryable<Team> GetAll()
		{
			return base.GetAll().Include(x => x.Members.Select(m => m.Skills));
		}

		public override IQueryable<Team> FindBy(Expression<Func<Team, bool>> predicate)
		{
			return base.FindBy(predicate).Include(x => x.Members.Select(m => m.Skills));
		}

		public void AddMember(int teamId, int employeeId)
		{
			Employee employee = Context.Employees.Single(x => x.Id == employeeId);
			Team team = Context.Teams.Include(x => x.Members).Single(x => x.Id == teamId);
			if (team.Members.All(x => x.Id != employee.Id))
			{
				team.Members.Add(employee);
				Context.SaveChanges();
			}
		}

		public void RemoveMember(int teamId, int employeeId)
		{
			Employee employee = Context.Employees.Single(x => x.Id == employeeId);
			Team team = Context.Teams.Include(x => x.Members).Single(x => x.Id == teamId);
			team.Members.Remove(employee);
			Context.SaveChanges();
		}
		
		public void SetMembers(int teamId, List<int> employeeIds)
		{
			List<Employee> employees = employeeIds.Select(employeeId => Context.Employees.Single(x => x.Id == employeeId)).ToList();
			Team team = Context.Teams.Include(x => x.Members).Single(x => x.Id == teamId);
			team.Members.Clear();
			foreach (var employee in employees)
			{
				if (team.Members.All(x => x.Id != employee.Id))
				{
					team.Members.Add(employee);
				}
			}
			Context.SaveChanges();
		}
	}

}

	