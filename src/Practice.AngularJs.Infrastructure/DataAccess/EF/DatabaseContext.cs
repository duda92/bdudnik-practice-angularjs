using System.Data.Entity;
using Practice.AngularJs.Domain.Employees;
using Practice.AngularJs.Domain.Skills;
using Practice.AngularJs.Domain.Teams;

namespace Practice.AngularJs.Infrastructure.DataAccess.EF
{
	public class DatabaseContext : DbContext
	{
		public DbSet<Employee> Employees { get; set; }

		public DbSet<Specialization> Specializations { get; set; }

		public DbSet<Team> Teams { get; set; }

		public DbSet<Skill> Skills { get; set; }

        public DbSet<Feedback> Feedbacks { get; set; }

        public DbSet<Job> Jobs { get; set; }

		public DatabaseContext() : base("ConnectionString")
		{
		}

		static DatabaseContext()
		{
			Database.SetInitializer(new DatabaseInitializer());
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Employee>().HasMany(j => j.Skills).WithOptional().HasForeignKey(x => x.EmployeeId);
			modelBuilder.Entity<Team>().HasMany(j => j.Members).WithMany(x => x.Teams).Map(cs =>
			{
				cs.MapLeftKey("TeamId");
				cs.MapRightKey("EmployeeId");
				cs.ToTable("TeamsEmployees");
			});
			;
			base.OnModelCreating(modelBuilder);
		}
	}
}