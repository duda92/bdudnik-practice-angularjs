﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Practice.AngularJs.Domain.Employees;
using Practice.AngularJs.Domain.Skills;
using Practice.AngularJs.Domain.Teams;
using Practice.AngularJs.Infrastructure.DataAccess.EF.Employees;
using Practice.AngularJs.Infrastructure.DataAccess.EF.Skills;
using Practice.AngularJs.Infrastructure.DataAccess.EF.Teams;
using Practice.AngularJs.Infrastructure.Utils;

namespace Practice.AngularJs.Infrastructure.DataAccess.EF
{
	public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<DatabaseContext>
	{
		protected override void Seed(DatabaseContext context)
		{
			InitializingByTestValues(context);
		}

		private void InitializingByTestValues(DatabaseContext context)
		{
			IEmployeeRepository employeeRepository = new EmployeeRepository();
			ITeamRepository teamRepository = new TeamRepository();
			ISpecializationRepository specializationRepository = new SpecializationRepository();
            IJobRepository jobRepository = new JobRepository();

            CreateJobs(jobRepository);

            CreateSpecializations(specializationRepository);

            const int employeesCount = 55;
            CreateEmployees(employeesCount, employeeRepository, specializationRepository, jobRepository);

			const int maxTeamMembers = 10;
			const int teamsCount = 5;

			CreateTeams(teamsCount, teamRepository, maxTeamMembers, employeeRepository, employeesCount);
		}

        private void CreateJobs(IJobRepository jobRepository)
	    {
            jobRepository.Add(new Job() { Title = "Developer" });
            jobRepository.Add(new Job() { Title = "Project Manager" });
            jobRepository.Add(new Job() { Title = "Product Manager" });
            jobRepository.Add(new Job() { Title = "QA" });
            jobRepository.Add(new Job() { Title = "CEO" });
            jobRepository.Save();
	    }

	    private void CreateSpecializations(ISpecializationRepository specializationRepository)
		{
			specializationRepository.Add(new Specialization() { Title = ".NET" });
			specializationRepository.Add(new Specialization() { Title = "Java" });
			specializationRepository.Add(new Specialization() { Title = "javascript" });
			specializationRepository.Add(new Specialization() { Title = "C++" });
			specializationRepository.Add(new Specialization() { Title = "Ruby" });
			specializationRepository.Add(new Specialization() { Title = "Phyton" });
			specializationRepository.Save();
		}


        private static void CreateEmployees(int employeesCount, IEmployeeRepository employeeRepository, ISpecializationRepository specializationRepository, IJobRepository jobRepository)
		{
			Random rand = new Random();
			List<Specialization> specializations = specializationRepository.GetAll().ToList();

			for (int i = 0; i < employeesCount; i++)
			{
			    int maxAgeCount = 100;
                int minAgeCount = 20;

                Employee employee = new Employee() { Name = "Employee" + i, Age = rand.Next(minAgeCount, maxAgeCount) };

			    int jobsCount = jobRepository.GetAll().Count();
			    int jobId = jobRepository.GetAll().ToList()[rand.Next(jobsCount)].Id;
			    employee.JobId = jobId;

                int specializationsCount = rand.Next(1, specializations.Count);
				for (int j = 0; j < specializationsCount; j++)
				{
					int specializationId = rand.Next(1, specializationsCount);
					employee.Skills.Add(new Skill()
					{
						Specialization = specializationRepository.FindBy(x => x.Id == specializationId).ToList().Single(),
						Grade = Enum.GetValues(typeof(Grade)).Cast<Grade>().OrderBy(x => rand.Next()).FirstOrDefault()
					});
				}

				const int maxFeedbacksCount = 3;
				int feedbacksCount = rand.Next(1, maxFeedbacksCount);
				RandomStringGenerator randomStringGenerator = new RandomStringGenerator();
				for (int j = 0; j < feedbacksCount; j++)
				{
					const int maxFeedbacksSize = 10;
					int feedbacksSize = rand.Next(1, maxFeedbacksSize);
					employee.Feedbacks.Add(new Feedback() { Text = randomStringGenerator.RandomString(feedbacksSize) });
				}
				employeeRepository.Add(employee);
				employeeRepository.Save();
			}
		}

		private static void CreateTeams(int teamsCount, ITeamRepository teamRepository, int maxTeamMembers,
			IEmployeeRepository employeeRepository, int employeesCount)
		{
			Random rand = new Random();
			for (int i = 0; i < teamsCount; i++)
			{
				Team team = new Team() { Title = "Team" + i };
				teamRepository.Add(team);
				teamRepository.Save();

				int teamMembersCount = rand.Next(maxTeamMembers);
				Employee member1 = employeeRepository.GetAll().First();
				//team.Members.Add(member1);
				teamRepository.AddMember(team.Id, member1.Id);

				for (int j = 0; j < teamMembersCount; j++)
				{
					Employee member = employeeRepository.GetAll().ToList()[rand.Next(employeesCount)];
					if (team.Members.All(x => x.Id != member.Id))
					{
						//team.Members.Add(member);
						teamRepository.AddMember(team.Id, member.Id);
					}
				}
				//teamRepository.Save();
			}
		}
	}

}
