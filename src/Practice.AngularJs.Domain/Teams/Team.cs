using System.Collections.Generic;
using System.Collections.ObjectModel;
using Practice.AngularJs.Domain.Employees;

namespace Practice.AngularJs.Domain.Teams
{
	public class Team
	{
		public Team()
		{
			Members = new Collection<Employee>();
		}

		public int Id { get; set; }

		public string Title { get; set; }

		public ICollection<Employee> Members { get; set; }
	}
}