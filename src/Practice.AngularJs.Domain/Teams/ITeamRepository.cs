﻿using System.Collections.Generic;

namespace Practice.AngularJs.Domain.Teams
{
	public interface ITeamRepository : IGenericRepository<Team>
	{
		void AddMember(int teamId, int employeeId);

		void SetMembers(int teamId, List<int> employeeIds);
		void RemoveMember(int teamId, int employeeId);
	}
}
