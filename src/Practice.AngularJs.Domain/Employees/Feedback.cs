﻿namespace Practice.AngularJs.Domain.Employees
{
    public class Feedback
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public string Text { get; set; }
    }
}