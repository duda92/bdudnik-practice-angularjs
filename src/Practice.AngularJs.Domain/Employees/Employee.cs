﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Practice.AngularJs.Domain.Skills;
using Practice.AngularJs.Domain.Teams;

namespace Practice.AngularJs.Domain.Employees
{
	public class Employee
	{
		public Employee()
		{
			Skills = new Collection<Skill>();
			Teams = new Collection<Team>();
			Feedbacks = new Collection<Feedback>();
		}

		public int Id { get; set; }


        public string Name { get; set; }


        public int JobId { get; set; }
        
        
        public Job Job { get; set; }


        public decimal Age { get; set; }


		public ICollection<Skill> Skills { get; set; }


		public ICollection<Team> Teams { get; set; }


		public ICollection<Feedback> Feedbacks { get; set; }

	}
}