﻿namespace Practice.AngularJs.Domain.Employees
{
    public class Job
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}