﻿namespace Practice.AngularJs.Domain.Employees
{
	public interface IEmployeeRepository : IGenericRepository<Employee>
	{
	}
}
