﻿namespace Practice.AngularJs.Domain.Skills
{
	public class Specialization 
	{
		public int Id { get; set; }

		public string Title { get; set; }

	}
}