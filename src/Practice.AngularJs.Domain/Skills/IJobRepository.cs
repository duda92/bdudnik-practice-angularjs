﻿using Practice.AngularJs.Domain.Employees;

namespace Practice.AngularJs.Domain.Skills
{
    public interface IJobRepository : IGenericRepository<Job>
    {

    }
}