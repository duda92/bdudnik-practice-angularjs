﻿using Practice.AngularJs.Domain.Employees;

namespace Practice.AngularJs.Domain.Skills
{
	public class Skill
	{
		public int Id { get; set; }

		public Grade Grade { get; set; }

		public int EmployeeId { get; set; }

		public virtual Employee Employee { get; set; }

		public int SpecializationId { get; set; }

		public virtual Specialization Specialization { get; set; }

	}
}