﻿namespace Practice.AngularJs.Domain.Skills
{
    public interface ISpecializationRepository : IGenericRepository<Specialization>
    {

    }
}
