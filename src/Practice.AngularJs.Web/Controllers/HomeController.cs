﻿using System.Web.Mvc;

namespace Practice.AngularJs.Web.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}