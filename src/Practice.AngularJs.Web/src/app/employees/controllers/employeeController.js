﻿angular.module('employees.controllers')
    .controller('employeeController', ['$scope', 'employeeViewModelFactory', 'restService', 'sharedDataService', '$state', function ($scope, employeeViewModelFactory, restService, sharedDataService, $state)
    {
        $scope.$state = $state;
        restService.getEmployees().then(
            function (resp)
            {
            	var employees = resp.data.employees;
            	var employeeViewModels = [];

            	angular.forEach(employees, function (value)
            	{
            		var employeeViewModel = employeeViewModelFactory.fromJsonObject(value);
            		employeeViewModels.push(employeeViewModel);
            	});

            	sharedDataService.setAllEmployees(employeeViewModels);
            	sharedDataService.setSelectedEmployees([]);

            	$scope.employeeViewModels = sharedDataService.getAllEmployees();
            	$scope.selectedEmployeeViewModels = sharedDataService.getSelectedEmployees([]);
            },
            function (err)
            {
            	console.error('ERR', err);
            });

    	$scope.$watch(sharedDataService.getSelectedTeam, function (newSelectedTeam)
    	{
    	    if (!$.isEmptyObject(newSelectedTeam))
    	    {
    	        sharedDataService.setSelectedEmployees(newSelectedTeam.members.slice());
    	    }
        });
    }]);