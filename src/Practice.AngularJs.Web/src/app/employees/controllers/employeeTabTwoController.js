﻿angular.module('employees.controllers')
    .controller('employeeTabTwoController', ['$scope', 'sharedDataService', function ($scope, sharedDataService)
    {
    	function updateIn()
    	{
    		$scope.currentTeam = sharedDataService.getSelectedTeam();
    		$scope.employeeToAdd = sharedDataService.getEmployeeToAdd();
			$scope.employeeViewModels = sharedDataService.getAllEmployees();
	    };

    	function updateOut()
    	{
    		sharedDataService.setEmployeeToAdd($scope.employeeToAdd);
    		sharedDataService.setAllEmployees($scope.employeeViewModels);
    	};

    	$scope.onAddEmployee = function ($event, employeeViewModel)
    	{
    		$scope.employeeToAdd = employeeViewModel;
    		updateOut();
    		$event.stopPropagation();
    	}

    	$scope.$watch(sharedDataService.getSelectedTeam, function ()
    	{
    	    updateIn();
    	});

    	$scope.onSelectEmployee = function (employeeViewModel)
    	{
    		employeeViewModel.selected = true;
    		$scope.employeeViewModels.forEach(function (currentemployeeViewModel)
    		{
    			if (employeeViewModel.id != currentemployeeViewModel.id)
    			{
    				currentemployeeViewModel.selected = false;
    			}
    		});
    		$scope.employeeToAdd = employeeViewModel;
    		updateOut();
    	}

    	$scope.isTeamMember = function (employeeViewModel)
    	{
    		if ($scope.currentTeam != null)
    		{
    			var isTeamMember = $scope.currentTeam.members.filter(function (member)
    			{
    				return member.id == employeeViewModel.id;
    			}).length == 1;
    			return isTeamMember;
    		}
    		return false;
    	}

    	updateIn();
    }]);