﻿angular.module('employees.controllers')
    .controller('employeeTabOneController',
    ['$scope', '$window', '$http', 'employeeViewModelFactory', 'restService', 'sharedDataService', function ($scope, $window, $http, employeeViewModelFactory, restService, sharedDataService)
    {
    	function updateIn()
    	{
    		$scope.employeeViewModels = sharedDataService.getAllEmployees();
    		$scope.selectedEmployeeViewModels = sharedDataService.getSelectedEmployees();
	    };

    	function updateOut()
    	{
    		sharedDataService.setSelectedEmployees($scope.selectedEmployeeViewModels);
    	};

    	$scope.onEmployeeSelected = function ()
        {
    		updateOut();
        }

        $scope.onEmployeeRemoved = function ()
        {
            updateOut();
        }

        $scope.onRefresh = function ()
        {
            updateOut();
            sharedDataService.setSyncTeamWithMembersFlag(true);
        };
        $scope.$watch(sharedDataService.getSelectedTeam, function ()
        {
            updateIn();
        });

        updateIn();
    }]);