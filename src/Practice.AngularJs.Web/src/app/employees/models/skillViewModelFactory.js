﻿angular.module("employees.models")
    .factory('skillViewModelFactory', ['specializationViewModelFactory', function (specializationViewModelFactory)
{

    /**
     * Constructor, with class name
     */
    function SkillViewModel(jsonData)
    {
        // Public properties, assigned to the instance ('this')
        this.id = jsonData.id;
        this.grade = jsonData.grade;
        this.specialization = specializationViewModelFactory.fromJsonObject(jsonData.specialization);
    }

    /**
     * Public method, assigned to prototype
     */
    SkillViewModel.prototype.toJsonObject = function ()
    {
        return {
            id: this.id,
            grade: this.grade,
            specialization: this.specialization.toJsonObject()
        };
    };

    SkillViewModel.prototype.displayText = function ()
    {
        return this.grade + ' ' + this.specialization.displayText();
    };

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    SkillViewModel.fromJsonObject = function (data)
    {
        return new SkillViewModel(data);
    };

    /**
     * Return the constructor function
     */
    return SkillViewModel;
}]);