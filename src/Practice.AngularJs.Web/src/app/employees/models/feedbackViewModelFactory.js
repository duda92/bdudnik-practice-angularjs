﻿angular.module("employees.models").factory('feedbackViewModelFactory', [function ()
{

    /**
     * Constructor, with class name
     */
    function FeedbackViewModel(jsonData)
    {
        // Public properties, assigned to the instance ('this')
        this.id = jsonData.id;
        this.text = jsonData.text;
    }

    /**
     * Public method, assigned to prototype
     */
    FeedbackViewModel.prototype.toJsonObject = function ()
    {
        return {
            id: this.id,
            text: this.text
        };
    };

    FeedbackViewModel.prototype.displayText = function ()
    {
        return this.text;
    };

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    FeedbackViewModel.fromJsonObject = function (data)
    {
        return new FeedbackViewModel(data);
    };

    /**
     * Return the constructor function
     */
    return FeedbackViewModel;
}]);