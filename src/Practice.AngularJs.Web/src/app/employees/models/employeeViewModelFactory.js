﻿angular.module("employees.models")
    .factory('employeeViewModelFactory', ['skillViewModelFactory', 'feedbackViewModelFactory', function (skillViewModelFactory, feedbackViewModelFactory)
{

    /**
     * Constructor, with class name
     */
    function EmployeeViewModel(jsonData)
    {

        var skills = jsonData.skills;
        var skillViewModels = [];

        angular.forEach(skills, function (value)
        {
            var skillViewModel = skillViewModelFactory.fromJsonObject(value);
            skillViewModels.push(skillViewModel);
        });

        var feedbacks = jsonData.feedbacks;
        var feedbackViewModels = [];

        angular.forEach(feedbacks, function (value)
        {
            var feedbackViewModel = feedbackViewModelFactory.fromJsonObject(value);
            feedbackViewModels.push(feedbackViewModel);
        });

        // Public properties, assigned to the instance ('this')
        this.id = jsonData.id;
        this.name = jsonData.name;
        this.skills = skillViewModels;
        this.feedbacks = feedbackViewModels;
        this.age = jsonData.age;
        this.job = jsonData.job;

        this.selected = false;
    }

    /**
     * Public method, assigned to prototype
     */
    EmployeeViewModel.prototype.toJsonObject = function ()
    {
        return {
            id: this.id,
            name: this.name,
            skills: this.skills.map(function (viewModel) { return viewModel.toJsonObject(); }),
            feedbacks: this.feedbacks.map(function (viewModel) { return viewModel.toJsonObject(); }),
            age: this.age,
            job: this.job
        };
    };

    EmployeeViewModel.prototype.displayText = function ()
    {
        return this.name + ' ' + ': ' + this.skills.map(function (viewModel) { return viewModel.displayText(); }).join();
    };

    EmployeeViewModel.prototype.employeeSkillsDisplayText = function ()
    {
        return this.skills.map(function (viewModel) { return viewModel.displayText(); }).join();
    };

    EmployeeViewModel.prototype.shortDisplayText = function ()
    {
        return this.name;
    };

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    EmployeeViewModel.fromJsonObject = function (data)
    {
        return new EmployeeViewModel(data);
    };

    /**
     * Return the constructor function
     */
    return EmployeeViewModel;
}]);