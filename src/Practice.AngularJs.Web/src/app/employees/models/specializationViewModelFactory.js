﻿angular.module("employees.models")
    .factory('specializationViewModelFactory', [function ()
{

    /**
     * Constructor, with class name
     */
    function SpecializationViewModel(jsonData)
    {
        // Public properties, assigned to the instance ('this')
        this.id = jsonData.id;
        this.title = jsonData.title;
    }

    /**
     * Public method, assigned to prototype
     */
    SpecializationViewModel.prototype.toJsonObject = function ()
    {
        return {
            id: this.id,
            title: this.title
        };
    };

    SpecializationViewModel.prototype.displayText = function ()
    {
        return this.title;
    };

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    SpecializationViewModel.fromJsonObject = function (data)
    {
        return new SpecializationViewModel(data);
    };

    /**
     * Return the constructor function
     */
    return SpecializationViewModel;
}]);