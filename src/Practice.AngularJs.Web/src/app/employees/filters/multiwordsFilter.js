﻿angular.module('employees.filters')
    .filter('multiwordsFilter', ['$filter', function (filter)
    {
    	var lowercaseFilter = filter('filter');
    	return function (list, search)
    	{
    		if (!search) return list;
    		var arrSearch = search.split(' ');
    		var lookups = [];

    		arrSearch.forEach(function (item)
    		{
    			var lookup = lowercaseFilter(list, item);
    			lookups.push(lookup);
    		});

    		var result = lookups.shift().filter(function (v)
    		{
    			return lookups.every(function (a)
    			{
    				return a.indexOf(v) !== -1;
    			});
    		});
    		return result;
    	};
    }]);