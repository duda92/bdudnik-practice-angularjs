﻿angular.module('employees.directives')
.directive('typeahead', ['$timeout', function ($timeout)
{
	return {
		restrict: 'AEC',
		scope: {

			allItems: '=',
			selectedItems: '=',

			placeholder: '@',
			onSelected: '&',
			onRemoved: '&'
		},
		link: function (scope, elem, attrs)
		{
			scope.$watchCollection('selectedItems', function (selectedItems)
			{
				var allItems = scope.allItems;
				if (typeof (allItems) != "undefined")
				{
					var selectedItemIds = selectedItems.map(function (v) { return v.id; });
					scope.notSelectedItems = allItems.filter(function (item)
					{
						return (selectedItemIds.indexOf(item.id) == -1);
					}).slice();
				}
			}, true);

			scope.selectItem = function (item)
			{
				scope.searchText = '';
				scope.current = 0;
				scope.selected = true;
				$timeout(function ()
				{
					scope.selectedItems.push(item);
					var index = scope.notSelectedItems.indexOf(item);
					scope.notSelectedItems.splice(index, 1);
                    scope.notSelectedItems.sort(utils.compareByName);
                    scope.onSelected.call(scope.$parent, item);
				}, 200);
			};

			scope.removeItem = function (item)
			{
			    var index = scope.selectedItems.indexOf(item);
			    scope.selectedItems.splice(index, 1);
				scope.notSelectedItems.push(item);
				scope.notSelectedItems.sort(utils.compareByName);

				scope.onRemoved.call(scope.$parent, item);
			};


			scope.current = 0;
			scope.selected = true;
			scope.isCurrent = function (index)
			{
				return scope.current == index;
			};
			scope.setCurrent = function (index)
			{
				scope.current = index;
			};
		},
		templateUrl: '/src/app/employees/directives/typeahead/typeahead.html'
	}
}]);