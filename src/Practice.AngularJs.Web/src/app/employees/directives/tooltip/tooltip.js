﻿angular.module('employees.directives').
directive('tooltip', [function ()
   {
		return {
   			link: function (scope, element, attributes)
   			{
   			    var tooltipSelector = attributes.tooltipSelector;
                element.bind('mouseover', function ()
                {
                    $(this).find(tooltipSelector).show();

   				});
   				element.bind('mouseleave', function ()
   				{
   				    $(this).find(tooltipSelector).hide();
   				});
   			}
   	};
   }]);
