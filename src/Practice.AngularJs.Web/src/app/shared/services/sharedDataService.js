﻿angular.module('shared.services')
    .factory('sharedDataService', [function ()
    {
        var Data =
       {
           SelectedTeam: {},
           Employees: [],

           SelectedEmployees: [],
           EmployeeToAdd: {}
       };

        var syncTeamWithMembersFlag = false;

        var service =
        {
            getSelectedTeam: function ()
            {
                return Data.SelectedTeam;
            },
            setSelectedTeam: function (team)
            {
                Data.SelectedTeam = team;
            },
            //------------------------------------------------------------

            //------------------------------------------------------------
            getAllEmployees: function ()
            {
                return Data.Employees;
            },
            setAllEmployees: function (employees)
            {
                Data.Employees = employees;
            },
            //------------------------------------------------------------


            //------------------------------------------------------------
            getSelectedEmployees: function ()
            {
                return Data.SelectedEmployees;
            },
            setSelectedEmployees: function (employees)
            {
                return Data.SelectedEmployees = employees;
            },

            //------------------------------------------------------------

            //------------------------------------------------------------
            getEmployeeToAdd: function ()
            {
                return Data.EmployeeToAdd;
            },
            setEmployeeToAdd: function (employee)
            {
                return Data.EmployeeToAdd = employee;
            },

            //------------------------------------------------------------
            getSyncTeamWithMembersFlag: function ()
            {
                return syncTeamWithMembersFlag;
            },
            setSyncTeamWithMembersFlag: function (value)
            {
                return syncTeamWithMembersFlag = value;
            },
            //------------------------------------------------------------
        };
        return service;
    }]);

