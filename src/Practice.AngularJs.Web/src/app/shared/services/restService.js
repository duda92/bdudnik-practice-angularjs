﻿angular.module('shared.services')
    .factory('restService', ['$http', function ($http)
    {
    	var employeesUrl = '/api/Employee/GetEmployeesList';
    	var teamsUrl = '/api/Team/GetTeamsList';
    	var addNewTeamUrl = '/api/Team/AddNew';
    	var updateTeamEmployeesUrl = '/api/Team/UpdateTeamEmployees';
    	var addEmployeeToTeamUrl = '/api/Team/addEmployeeToTeam';
    	var removeEmployeeFromTeamUrl = '/api/Team/removeEmployeeFromTeam';

    	var restService = {};

    	restService.getEmployees = function ()
    	{
    		return $http.get(employeesUrl);
    	};

    	restService.getTeams = function ()
    	{
    		return $http.get(teamsUrl);
    	};

    	restService.addNewTeam = function (name)
    	{
    		return $http.post(addNewTeamUrl, { name: name });
    	};

    	restService.updateTeamEmployees = function (team, employees)
    	{
    		var updateTeamEmployeesDTOInput = {
    			teamId: team.id,
    			employeeIds: employees.map(function (employee) { return employee.id; })
    		};
    		return $http.post(updateTeamEmployeesUrl, updateTeamEmployeesDTOInput);
    	}

    	restService.addEmployeeToTeam = function (team, employee)
    	{
    		var addEmployeeToTeamDTOInput = {
    			teamId: team.id,
    			employeeId: employee.id
    		};
    		return $http.post(addEmployeeToTeamUrl, addEmployeeToTeamDTOInput);
    	};

    	restService.removeEmployeeFromTeam = function (team, employee)
    	{
    		var removeEmployeeFromTeamDTOInput = {
    			teamId: team.id,
    			employeeId: employee.id
    		};
    		return $http.post(removeEmployeeFromTeamUrl, removeEmployeeFromTeamDTOInput);
    	};
    	return restService;
    }]);