﻿angular.module("teams.models").
    factory('teamViewModelFactory', ['employeeViewModelFactory', function (employeeViewModelFactory)
{

    /**
     * Constructor, with class name
     */
    function TeamViewModel(jsonData)
    {

        var employees = jsonData.members;
        var employeeViewModels = [];

        angular.forEach(employees, function (value)
        {
            var employeeViewModel = employeeViewModelFactory.fromJsonObject(value);
            employeeViewModels.push(employeeViewModel);
        });

        this.id = jsonData.id;
        this.title = jsonData.title;
        this.members = employeeViewModels;

        this.selected = false;
        this.expanded = false;
    }

    /**
     * Public method, assigned to prototype
     */
    TeamViewModel.prototype.toJsonObject = function ()
    {
        return {
            id: this.id,
            title: this.title,
            members: this.members
        };
    };

    /**
     * Static method, assigned to class
     * Instance ('this') is not available in static context
     */
    TeamViewModel.fromJsonObject = function (data)
    {
        return new TeamViewModel(data);
    };

    /**
     * Return the constructor function
     */
    return TeamViewModel;
}]);
