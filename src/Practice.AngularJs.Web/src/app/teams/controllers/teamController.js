﻿angular.module('teams.controllers')
    .controller('teamController', ['$scope', '$window', '$http', 'teamViewModelFactory', 'restService', 'sharedDataService', '$state', function ($scope, $window, $http, teamViewModelFactory, restService, sharedDataService, $state)
    {
    	function onTeamMembersUpdated(updateTeamJson)
    	{
    		var updatedTeamViewModel = teamViewModelFactory.fromJsonObject(updateTeamJson);

    		var teamViewModelToRefresh = $scope.teamViewModels.filter(function (viewModel) { return viewModel.id == updatedTeamViewModel.id; })[0];
    		var index = $scope.teamViewModels.indexOf(teamViewModelToRefresh);
    		updatedTeamViewModel.selected = true;
    		updatedTeamViewModel.expanded = teamViewModelToRefresh.expanded;
    		$scope.teamViewModels[index] = updatedTeamViewModel;

	        sharedDataService.setSelectedTeam(updatedTeamViewModel);
    		sharedDataService.setSyncTeamWithMembersFlag(false);
    	}

    	restService.getTeams().then(
            function (resp)
            {
            	var teams = resp.data.teams;
            	var teamViewModels = [];

            	angular.forEach(teams, function (value)
            	{
            		var teamViewModel = teamViewModelFactory.fromJsonObject(value);
            		teamViewModels.push(teamViewModel);
            	});
            	$scope.teamViewModels = teamViewModels;
            },
            function (err)
            {
            	console.error('ERR', err);
            });

    	$scope.toggleTeamSelection = function (teamViewModel)
    	{
    		teamViewModel.selected = true;
    		angular.forEach($scope.teamViewModels, function (currentTeamViewModel)
    		{
    			if (teamViewModel.id != currentTeamViewModel.id)
    			{
    				currentTeamViewModel.selected = false;
    			}
    		});
    		sharedDataService.setSelectedTeam(teamViewModel);
    		$state.go('activeTeam.tabOne');
    	}

    	$scope.onRemoveEmployee = function (employeeViewModel)
    	{
    		var team = sharedDataService.getSelectedTeam().toJsonObject();
    		var employee = employeeViewModel.toJsonObject();

    		restService.removeEmployeeFromTeam(team, employee).
                success(function (data)
    			{
                	onTeamMembersUpdated(data.team);
                }).
                error(function ()
    			{ 

                });
    	}

    	$scope.$watch(sharedDataService.getSyncTeamWithMembersFlag, function (syncTeamWithMembersFlag)
    	{
    	    if (!syncTeamWithMembersFlag)
    	    {
    	        return;
    	    }
	        var team = sharedDataService.getSelectedTeam().toJsonObject();
    	    var employees = sharedDataService.getSelectedEmployees().map(function (viewModel) { return viewModel.toJsonObject(); });
    	    restService.updateTeamEmployees(team, employees).
                success(function (data)
                {
                    onTeamMembersUpdated(data.team);
                }).
                error(function ()
                {

                });
    	});

    	$scope.$watch(sharedDataService.getEmployeeToAdd, function ()
    	{
    	    if ($.isEmptyObject(sharedDataService.getSelectedTeam()) || $.isEmptyObject(sharedDataService.getEmployeeToAdd()))
    	    {
	            return;
	        }
    		var team = sharedDataService.getSelectedTeam().toJsonObject();
    		var employee = sharedDataService.getEmployeeToAdd().toJsonObject();

    		restService.addEmployeeToTeam(team, employee).
                success(function (data)
                {
                	onTeamMembersUpdated(data.team);
                }).
                error(function ()
                {

                });
    	});

    	$scope.toggleExpand = function ($event, teamViewModel)
    	{
    		teamViewModel.expanded = !teamViewModel.expanded;
    	}

    	$scope.addNewTeam = function (name)
    	{
    		restService.addNewTeam(name).
            success(function (data)
            {
            	var teamViewModel = teamViewModelFactory.fromJsonObject(data.team);
            	$scope.teamViewModels.push(teamViewModel);
            	$scope.newTeamName = '';
            }).
            error(function ()
            {

            });
    	}
    }]);