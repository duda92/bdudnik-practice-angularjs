﻿angular.module('shared.services', []);

angular.module('employees.models', []);
angular.module('employees.controllers', ['employees.models', 'shared.services']);
angular.module('employees.filters', []);
angular.module('employees.directives', []);

angular.module("teams.models", ['employees.models']);
angular.module('teams.controllers', ['teams.models', 'shared.services']);

var app = angular.module('app', [
    
    'ui.router',
    'ngAnimate',

    'shared.services',

    'teams.controllers',
    'teams.models',

    'employees.controllers',
    'employees.models',
    'employees.filters',
    'employees.directives'

]);

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider)
{
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state("start",
        {
            url: "/",
            template: '<h1 class="page-header" class="team-title-placeholder">"Please choose a team..."</h1>'
        })
        .state("activeTeam",
        {
            url: "/",
            templateUrl: '/src/app/employees/views/EmployeesMain.html'
        })
       .state('activeTeam.tabOne',
       {
           url: 'one',
           templateUrl: '/src/app/employees/views/TabOne.html'
       })
       .state('activeTeam.tabTwo',
       {
           url: 'two',
           templateUrl: '/src/app/employees/views/TabTwo.html'
       });
}
]);

app.run(['$state', function ($state) {
    $state.transitionTo('start');
}])