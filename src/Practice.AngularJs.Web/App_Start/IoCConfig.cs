﻿using Autofac;
using Practice.AngularJs.Web.Application.Employees;

namespace Practice.AngularJs.Web
{
	public static class IoCConfig
	{
		public static void RegisterDependencies(ContainerBuilder builder)
		{
			builder.RegisterType<EmployeeService>().As<IEmployeeService>();
            builder.RegisterType<TeamService>().As<ITeamService>();
		}
	}
}