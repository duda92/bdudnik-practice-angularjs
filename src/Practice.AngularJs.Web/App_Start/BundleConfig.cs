﻿using System.Web.Optimization;

namespace Practice.AngularJs.Web
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new StyleBundle("~/styles").IncludeDirectory("~/Assets/styles", "*.css", true));
            bundles.Add(new StyleBundle("~/bootstrap").IncludeDirectory("~/Assets/bootstrap/css", "*.css", true));


			bundles.Add(new ScriptBundle("~/ng").Include(
						"~/Assets/ng/angular.min.js",
						"~/Assets/ng/angular-route.min.js",
						"~/Assets/ng/angular-cookies.min.js",
                        "~/Assets/ng/angular-animate.min.js",
                        "~/Assets/ng/angular-ui-router.js"));
			bundles.Add(new ScriptBundle("~/jquery").Include("~/Assets/jquery/jquery.min.js"));

			bundles.Add(new ScriptBundle("~/app").IncludeDirectory("~/src/app", "*.js", true));

			BundleTable.EnableOptimizations = false;
		}
	}
}