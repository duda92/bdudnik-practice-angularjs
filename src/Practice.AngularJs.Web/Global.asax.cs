﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Practice.AngularJs.Web.Application;

namespace Practice.AngularJs.Web
{
	public class MvcApplication : HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
			var builder = new ContainerBuilder();

			builder.RegisterModule(new PracticeAngularJsWebApplicationModule());
			builder.RegisterModule(new PracticeAngularJsWebModule());
			
			var container = builder.Build();
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
			GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
		}
	}
}