﻿using System.Web.Http;
using Practice.AngularJs.Web.Application.Employees;
using Practice.AngularJs.Web.Application.Employees.DTOs;

namespace Practice.AngularJs.Web.Api.Controllers
{
	public class EmployeeController : ApiController
	{
		private readonly IEmployeeService _employeeService;

		public EmployeeController(IEmployeeService employeeService)
		{
			_employeeService = employeeService;
		}

		public EmployeesListDTOOutput GetEmployeesList(GetEmployeeListInput employeeListInput)
		{
			return _employeeService.GetEmployeeList(employeeListInput);
		}
	}
}