﻿using System.Web.Http;
using Practice.AngularJs.Web.Application.Employees;
using Practice.AngularJs.Web.Application.Employees.DTOs;

namespace Practice.AngularJs.Web.Api.Controllers
{
	public class TeamController : ApiController
	{
		private readonly ITeamService _teamService;

        public TeamController(ITeamService teamService)
		{
			_teamService = teamService;
		}

		public TeamListDTOOutput GetTeamsList(GetTeamListDTOInput teamsListDTOInput)
		{
			return _teamService.GetTeamsList(teamsListDTOInput);
		}

	    [HttpPost]
        public NewTeamDTOOutput AddNew(NewTeamDTOInput newTeamDTOInput)
	    {
            return _teamService.NewTeam(newTeamDTOInput);
	    }

	    [HttpPost]
	    public UpdateTeamEmployeesDTOOutput UpdateTeamEmployees(UpdateTeamEmployeesDTOInput updateTeamEmployeesDTOInput)
	    {
            return _teamService.UpdateTeamEmployees(updateTeamEmployeesDTOInput);
	    }

		[HttpPost]
		public AddEmployeeToTeamDTOOutput AddEmployeeToTeam(AddEmployeeToTeamDTOInput addEmployeeToTeamDTOInput)
		{
			return _teamService.AddEmployeeToTeam(addEmployeeToTeamDTOInput);
		}
		
		[HttpPost]
		public RemoveEmployeeFromTeamDTOOutput RemoveEmployeeFromTeam(RemoveEmployeeFromTeamDTOInput removeEmployeeFromTeamDTOInput)
		{
			return _teamService.RemoveEmployeeFromTeam(removeEmployeeFromTeamDTOInput);
		}
	}
}